/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Final1102;

import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lendle
 */

/*
 Question 1 (10%): 加入所有必要的 annotation，讓這個 service 類別能夠運作
*/
@Service
public class FileItemService {
    private EntityManager entityManager=null;
    
    public void addFileItem(FileItem o){
        if(o.getId()==null){
            o.setId(UUID.randomUUID().toString());
        }
        entityManager.persist(o);
    }
    
    public void editFileItem(FileItem o){
        FileItem item=getFileItem(o.getId());
        if(o.getContent()!=null){
            if(o.getContent().getId()==null){
                o.getContent().setId(UUID.randomUUID().toString());
            }
            item.setContent(o.getContent());
        }
        entityManager.merge(item);
    }
    
    public FileItem getFileItem(String id){
        return entityManager.find(FileItem.class, id);
    }
    
    public void delteFileItem(String id){
        FileItem item=getFileItem(id);
        deleteFileItem(item);
    }
    
    private void deleteFileItem(FileItem item){
        entityManager.remove(item);
        if(item.isFile()==false){
            Query query=entityManager.createNamedQuery("FileItem.findByParentId");
            query.setParameter("parentId", item.getId());
            List<FileItem> children=query.getResultList();
            for(FileItem o : children){
                deleteFileItem(o);
            }
        }
    }
    
    public List<FileItem> getFiles(){
        return entityManager.createNamedQuery("FileItem.findAll").getResultList();
    }
}
