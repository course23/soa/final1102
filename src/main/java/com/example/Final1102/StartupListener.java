/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Final1102;

import javax.persistence.EntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author lendle
 */
@WebListener
@Component
public class StartupListener implements ServletContextListener {

    @Autowired
    private EntityManager entityManager = null;

    @Transactional
    public void contextInitialized(ServletContextEvent sce) {
        FileItem item = entityManager.find(FileItem.class, "-1");
        if (item == null) {
            FileItem item1 = new FileItem();
            item1.setId("-1");
            item1.setName("Root");
            item1.setFile(false);
            entityManager.persist(item1);
        }
    }
}
