/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Final1102;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author lendle
 */

/*
Question 2（10%）: 加入所有必要的 annotation，讓這個 entity 類別能夠運作
*/
@NamedQueries({
    @NamedQuery(name = "FileItem.findAll", query="select o from FileItem o"),
    @NamedQuery(name="FileItem.findByParentId", query = "select o from FileItem o where o.parentId=:parentId")
})
public class FileItem {
    private String id=null;
    private String name=null;
    private String parentId=null;
    @OneToOne(cascade = CascadeType.ALL)
    private FileItemContent content=null;
    private boolean file=false;

    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public FileItemContent getContent() {
        return content;
    }

    public void setContent(FileItemContent content) {
        this.content = content;
    }

    
}
