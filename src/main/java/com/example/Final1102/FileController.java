/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.Final1102;

import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lendle
 */
@RestController
public class FileController {
    /*
    Question 3 (10%): 適當的宣告 entityManager 及 fileItemService，並加入 annotation
    請務必在下列區域中作答
    */
    //Start of Question 3
    
    //End of Question 3
    
    @PostMapping("/api/file")
    public void addFileItem(@RequestBody FileItem o){
        //Question 4(10%): 完成 addFileItem，請務必在下列區域中作答
        
        //Start of Question 4
        
        //End of Question 4
    }
    
    @PutMapping("/api/file")
    public void editFileItem(@RequestBody FileItem o){
        //Question 5(10%): 完成 editFileItem，請務必在下列區域中作答
        
        //Start of Question 5
        
        //End of Question 5
    }
    
    @GetMapping("/api/file/id/{id}")
    public FileItem getFileItem(@PathVariable String id){
        //Question 6(10%): 完成 getFileItem，請務必在下列區域中作答
        
        //Start of Question 6
        
        //End of Question 6
    }
    
    @DeleteMapping("/api/file/id/{id}")
    public void delteFileItem(@PathVariable String id){
        //Question 7(10%): 完成 delteFileItem，請務必在下列區域中作答
        
        //Start of Question 7
        
        //End of Question 7
    }
    
    @GetMapping("/api/files")
    public List<FileItem> getFiles(){
        //Question 8(10%): 完成 getFiles，請務必在下列區域中作答
        
        //Start of Question 8
        
        //End of Question 8
    }
}
