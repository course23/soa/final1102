package com.example.Final1102;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Final1102Application {

	public static void main(String[] args) {
		SpringApplication.run(Final1102Application.class, args);
	}

}
